<?php

require 'src/Commands/AddOperator.php';
require 'src/Commands/SubtractOperator.php';
require 'src/Commands/MultiplyOperator.php';
require 'src/Commands/DivideOperator.php';
require 'src/Commands/PowerOperator.php';

return [
    // TODO : Add list of commands here
    'add' => new AddOperator(),
    'subtract' => new SubtractOperator(),
    'multiply' => new MultiplyOperator(),
    'divide' => new DivideOperator(),
    'power' => new PowerOperator()
];
