<?php

use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;

Class SubtractOperator extends Command{

    protected $commandName = "subtract";
    protected $commandDescription = "Subtract inputted numbers";

    protected $commandArgumentName = "nums";
    protected $commandArgumentDescription = "Inputted number (minimum 2 numbers)";

    protected function configure(){

        $this->setName($this->commandName)
        ->setDescription($this->commandDescription)
        ->addArgument(
            $this->commandArgumentName,
            InputArgument::IS_ARRAY,
            $this->commandArgumentDescription
        );
    }

    protected function execute(InputInterface $input, OutputInterface $output){
        $numbers = $input->getArgument($this->commandArgumentName);
        $total = $numbers[0];

        if(count($numbers) > 1){
            
            for($i=1; $i<count($numbers); $i++) $total -= $numbers[$i];

            $output->writeln(implode(" - ",$numbers)." = ".$total);

        }else $output->writeln("Please input at least 2 numbers");

    }
}
